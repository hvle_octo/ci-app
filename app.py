"""This is a simple API"""

import os, sys
# import pickle Make bandit not happy
from http import HTTPStatus
from flask import Flask
from werkzeug.exceptions import HTTPException

APP = Flask(__name__)


@APP.route('/')
def hello():
    """Simple endpoint for /"""
    return 'Hello, App!'


@APP.route('/version')
def version():
    """Simple endpoint for /version"""
    return os.getenv('APP_VERSION','No version')


@APP.errorhandler(HTTPException)
def handle_http_exception(exception: HTTPException):
    """Handler for HTTPException"""
    return (exception.description, exception.code)


@APP.errorhandler(Exception)
def handle_exception():
    """Handler for generic Exception"""
    return (HTTPStatus.INTERNAL_SERVER_ERROR.description,
            HTTPStatus.INTERNAL_SERVER_ERROR)
